export default function() {
	this.get('/songs', function() {
		return {
			data: [{
				type: 'songs',
				id: 1,
				attributes: {
					title: 'Broken Film',
					artist: 'Tiny Little Fracture',
					path: 'https://soundcloud.com/tinylittlefracture/broken-film-original-version',
					player: 'soundcloud'
				}
			},{
				type: 'songs',
				id: 2,
				attributes: {
					title: 'Composition',
					artist: 'PaaM!',
					path: 'https://soundcloud.com/allex-punct/composition',
					player: 'soundcloud'
				}
			},{
				type: 'songs',
				id: 3,
				attributes: {
					title: 'II (Interlude)',
					artist: 'Tiny Little Fracture',
					path: 'https://soundcloud.com/tinylittlefracture/ii-interlude',
					player: 'soundcloud'
				}
			}]
		}
	});
}