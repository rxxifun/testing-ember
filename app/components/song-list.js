import Ember from 'ember';

export default Ember.Component.extend({
	isPathShowing: true,
	actions: {
		pathShow() {
			this.set('isPathShowing', true);
		},
		pathHide() {
			this.set('isPathShowing', false);
		}
	}
});
